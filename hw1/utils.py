import heapq
import math
from typing import Any

import numpy as np
import scipy


def avg(x):
    return sum(x) / len(x)


def frequencies_percent(data: str) -> list[float]:
    """Normalize count_occurrences() to 100"""

    vals = pdf_estimate(data)
    return [100 * val for val in vals.values()]


def pdf_estimate_array(data: str) -> list[float]:
    """Normalize count_occurrences() to 1"""

    histogram = count_occurrences(data)
    total = sum(histogram.values())
    char_list = all_chars()

    # check for missing chars, if any
    if any((hist_char not in char_list) for hist_char in histogram.keys()):
        missing_chars = set(histogram.keys()) - set(char_list)
        print(f"Error: runaway characters: {missing_chars}")

    return [histogram[val] / total for val in all_chars()]


def pdf_estimate(data: str) -> dict[chr, float]:
    """Normalize count_occurrences() to 1"""

    histogram = count_occurrences(data)
    total = sum(histogram.values())
    char_list = all_chars()

    # check for missing chars, if any
    if any((hist_char not in char_list) for hist_char in histogram.keys()):
        missing_chars = set(histogram.keys()) - set(char_list)
        print(f"Error: runaway characters: {missing_chars}")

    return {val: histogram[val] / total for val in all_chars()}


def count_occurrences(data: str):
    """How many times each char is present. Returns as dictionary"""

    dict = {}
    for char in data:
        if char in dict:
            dict[char] += 1
        else:
            dict[char] = 1

    return dict


def count_occurences_array(data: str):
    a = count_occurrences(data)

    return [a[c] for c in all_chars()]


def all_chars() -> list[str]:
    """A list of all possible chars, sorted"""

    return [' '] + [chr(i) for i in range(ord('a'), ord('z') + 1)]


def entropy(pdf: dict[Any, float], d=2):
    """d-ary entropy calculation of pdf"""

    return -sum(px * math.log(px, d) for px in pdf.values())


class Symbol:
    def __init__(self, symbol, freq, left=None, right=None):
        # frequency of symbol
        self.freq = freq

        # symbol name (character)
        self.symbol = symbol

        # node left of current node
        self.left = left

        # node right of current node
        self.right = right

        # tree direction (0/1)
        self.huff = ''

        # result
        self.new_val = ''

    def __lt__(self, nxt):
        return self.freq < nxt.freq

    def __str__(self):
        return f"{self.symbol} {self.freq:.4f}: L=({self.left}) R=({self.right})"

    # codes for all symbols in the newly
    # created Huffman tree
    def assign_strings(self, val='', print_codes: bool = False) -> dict[chr, str]:
        """
        When the tree is complete, recursively attach codes starting with empty string.
        Step 1 adds the new character to the code
        Step
        """

        # 1) huffman code for current node
        self.new_val = val + str(self.huff)

        # if node is edge node then
        if not self.left and not self.right:

            # try to print it
            if print_codes:
                print(f"{self.symbol} -> {self.new_val}")

            # return the code in a dictionary
            return {self.symbol: self.new_val}

        values = {}

        # if node is not an edge node
        # then traverse inside it
        # also accumulate already done leaf nodes and their code strings
        if self.left:
            values |= self.left.assign_strings(self.new_val, print_codes)

        if self.right:
            values |= self.right.assign_strings(self.new_val, print_codes)

        return values


def huffman(pdf: dict[chr, float]) -> dict[chr, str]:
    """
    derive a huffman encoding from a dictionary of frequencies of characters
    there can be multiple huffman encodings, this is just one
    Reference implementation:  by Aashish Barnwal
    https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/
    """

    # 1) while there is more than one Symbol,
    # 2) merge two least common into one (and sum their chances)
    # 3) when there is only one
    # 4) assign it an empty string
    # 5) unpack

    # list containing unused nodes
    nodes = [Symbol(x, pdf[x]) for x in pdf]
    heapq.heapify(nodes)

    while len(nodes) > 1:
        # sort all the nodes in ascending order
        # based on their frequency
        left = heapq.heappop(nodes)
        right = heapq.heappop(nodes)

        if abs(left.freq - right.freq) < 0.000000001:
            print(f"Warning: there might be two equivalent Huffman codes: {left} {right}")

        # assign directional value to these nodes
        left.huff = 0
        right.huff = 1

        # combine the 2 smallest nodes to create
        # new node as their parent
        newNode = Symbol(left.symbol + right.symbol, left.freq + right.freq, left, right)
        heapq.heappush(nodes, newNode)

    # Huffman Tree is ready!
    result = nodes[0].assign_strings(print_codes=False)
    return result


def mean_length(code: dict[chr, str], pdf: dict[chr, float]):
    return sum(len(code[x]) * pdf[x] for x in pdf.keys())


def count_words(text: str):
    """Count how many space-delimited words does a text have. Ignores leading and trailing whitespace."""

    return len(get_words(text))


def get_words(text: str) -> list[str]:
    """Extract a list of words for simpler use"""

    return text.strip().split()


def count_word_lengths(text: str, default_len=1) -> list[int]:
    """
    Create a histogram-like list of word lengths as a list with length equal to the longest word + 1
    List[i] = # of words with length i. List[0] = 0.
    """

    counter = [0] * default_len
    for word in get_words(text):
        i = len(word)

        # expand list
        if i >= len(counter):
            counter += [0] * (i - len(counter) + 1)

        # increment value in list
        counter[i] += 1

    return counter


def normalized_word_lengths(text: str, longest_word_length: int) -> dict[Any, float]:
    """Returns a normalized dict of word lengths"""

    values = count_word_lengths(text)
    n = count_words(text)

    return {i: values[i] / n * 100 if i < len(values) else 0 for i in range(1, longest_word_length + 1)}


def get_longest_word_length(text: str) -> int:
    """Quick and dirty method to get length of longest word"""
    return len(count_word_lengths(text)) - 1


def estimate_expected_value(text: str) -> float:
    """ average of all word lengths """

    words = get_words(text)
    n = count_words(text)

    return sum(len(word) for word in words) / count_words(text)


def estimate_variance(text: str) -> float:
    """ vyberovy rozptyl s^2 """

    words = get_words(text)
    n = count_words(text)
    avg = estimate_expected_value(text)

    s2 = sum((len(word) - avg) ** 2 for word in words) / (n - 1)
    return s2


def count_transitions(text: str, FROM: chr, TO: chr) -> int:
    """example: from=A, to=B -> count transitions from A to B"""

    return text.count(FROM + TO)


def transition_matrix(text: str) -> list[list[float]]:
    """Chance for each transition to occur"""

    chars = all_chars()
    transition_occurence_matrix = [[count_transitions(text, FROM, TO)
                                    for TO in chars] for FROM in chars]
    sums_per_row = [sum(line) for line in transition_occurence_matrix]
    return [[value / sums_per_row[i] for value in row]
            for i, row in enumerate(transition_occurence_matrix)]


def get_stationary(text: str, p0=None) -> list[float]:
    """Stacionarni rozdeleni z daneho p0"""

    if p0 is None:
        p0 = [1 / len(all_chars()) for _ in range(len(all_chars()))]

    matrix = np.asarray(transition_matrix(text))

    p_old = p0
    p1 = np.matmul(p0, matrix)
    while not np.allclose(p_old, p1, atol=1e-15):
        p_old = p1
        p1 = np.matmul(p1, matrix)
    return p1


def gamma_dist(a, p, n=1):
    if n == 1:
        return scipy.stats.gamma.rvs(p, loc=0, scale=1 / a, size=1)[0]
    else:
        return scipy.stats.gamma.rvs(p, loc=0, scale=1 / a, size=n)



def exp_dist(la, n=1):
    if n == 1:
        return np.random.default_rng().exponential(scale=1/la, size=n)[0]
    else:
        return np.random.default_rng().exponential(scale=1/la, size=n)
