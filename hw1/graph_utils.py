from typing import Any

from matplotlib import pyplot as plt

from utils import frequencies_percent


def frequency_histogram(fig, histogram: dict[Any], name: str, which: int, possible_values: list[Any]):
    """
    Plot a histogram
    w/ help from https://stackoverflow.com/a/38178031/6123979
    which: 1 -- left, 2 -- right
    """

    ax = fig.add_subplot(1, 2, which)

    total = sum(histogram.values())

    frequencies = [histogram[val] / total * 100 for val in possible_values]

    x_coordinates = range(len(possible_values))
    ax.bar(x_coordinates, frequencies, align='center')

    plt.xlabel("word length")
    plt.ylabel("frequency (%)")

    ax.set_title(name)

    ax.xaxis.set_major_locator(plt.FixedLocator(x_coordinates))
    ax.xaxis.set_major_formatter(plt.FixedFormatter(possible_values))

def frequency_histogram_comparative(data1, data2, possible_values: list[Any]):
    """
    Plot both histograms next to each other
    """

    fig = plt.figure(figsize=(10, 6), dpi=100)
    ax = fig.add_subplot(1, 1, 1)

    values = possible_values
    x_coordinates = range(len(values))

    frequencies1 = frequencies_percent(data1[2])
    frequencies2 = frequencies_percent(data2[2])

    ax.bar(x_coordinates, frequencies1, align='center', color='b', alpha=.4, label=f"{data1[1]}")
    ax.bar(x_coordinates, frequencies2, align='center', color='r', alpha=.4, label=f"{data2[1]}")

    plt.xlabel("word length")
    plt.ylabel("frequency (%)")

    ax.legend()

    ax.set_title(f"{data1[1]} vs {data2[1]}")

    ax.xaxis.set_major_locator(plt.FixedLocator(x_coordinates))
    ax.xaxis.set_major_formatter(plt.FixedFormatter(values))
    plt.show()

