import numpy as np
import scipy
from matplotlib import pyplot as plt
from utils import *
from graph_utils import *
import scipy as sp
import seaborn as sns

# rozdeleni
la = 10  # poisson
a = 4
p = 2




def generate_timeline(t):
    ins = exp_dist(la, int(1.2*la*t))  # more than enough
    outs = gamma_dist(a, p, int(1.2*la*t))

    in_times = [sum(ins[:i + 1]) for i in range(len(ins) - 1)]
    in_times = [time for time in in_times if time <= t]

    out_times = [in_times[i] + outs[i] for i in range(len(in_times))]
    out_times = [time for time in out_times if time <= t]

    # sequence of events = tuples (time, +1/-1)
    event_times = [(time, +1) for time in in_times] + [(time, -1) for time in out_times]
    event_times.sort(key=lambda tup: tup[0])

    return event_times


def show_trajectory(timelines: list[list[Any]], max_t, alpha=0.05):
    fig, ax = plt.subplots(1, 1)

    for event_series in timelines:
        current_count = [0] + [sum(event[1] for event in event_series[:i]) for i in range(len(event_series))]
        event_times = [event[0] for event in event_series]

        current_count_from = [0] + event_times
        current_count_to = event_times + [max_t]

        # plt.plot(event_times, current_count, linewidth=0.1, color='blue', alpha=0.1)
        ax.hlines(current_count, current_count_from, current_count_to, linewidth=4, color='blue', alpha=alpha)

    ax.set_xlabel("t (s)")
    ax.set_ylabel("počet požadavků")
    plt.show()

def pt_1():
    """
    (2b) Simulujte jednu trajektorii {Nt​(ω)∣t∈(0,10 s)}.
    Průběh trajektorie graficky znázorněte.
    Řádně popište princip generování této trajektorie!
    """

    t = 10

    event_times = generate_timeline(t)
    show_trajectory([event_times], t)

def pt_2():
    """
    (2b) Simulujte n=500 nezávislých trajektorií pro t∈(0,100).
    Na základě těchto simulací odhadněte rozdělení náhodné veličiny N100.
    """

    t = 100
    n = 500

    event_times = [generate_timeline(t) for x in range(n)]
    # show_trajectory(event_times, t)
    # show_trajectory(event_times, t, 0.09)
    # show_trajectory(event_times, t, 0.02)

    N100s = [sum(event[1] for event in events) for events in event_times]
    print(N100s)

    min_n = 0
    max_n = max(N100s)

    ran = range(min_n, max_n + 1)
    counts = [len([N100 for N100 in N100s if N100 == val]) * 100 / n for val in ran]

    print([len([N100 for N100 in N100s if N100 == val]) for val in ran])

    fig = plt.figure(figsize=(10, 6), dpi=100)
    ax = fig.add_subplot(1, 1, 1)

    ax.bar(ran, counts, align='center')
    # ax.bar(x_coordinates, frequencies1, align='center', color='b', alpha=.4, label=f"{data1[1]}")

    ax.xaxis.set_major_locator(plt.FixedLocator(range(len(ran))))
    ax.xaxis.set_major_formatter(plt.FixedFormatter([str(x) for x in ran]))

    ax.set_xlabel("počet požadavků v čase t=100 s")
    ax.set_ylabel("četnost daného počtu (%)")
    # plt.show()

    t = np.arange(0, 20, 0.1)
    from scipy.special import factorial
    d = np.exp(-5) * np.power(5, t) / factorial(t) * 100
    plt.plot(t, d, 'bs')
    plt.show()


def pt_3():
    """"""

    data = [4, 16, 48, 78, 82, 75, 77, 46, 40, 16, 12, 3, 2, 1]

    # is this Po(5) ?

    # yes (method: kouknu-vidim)

    # done (jk)



def hw4(data1, data2):
    """
    Uvažujte model hromadné obsluhy M∣G∣∞M∣G∣∞.
    • Požadavky přicházejí podle Poissonova procesu s intenzitou λ=10 s−1.
    • Doba obsluhy jednoho požadavku (v sekundách) má rozdělení S∼Ga(4,2), tj. Gamma s parametry a=4, p=2.
    • Časy mezi příchody a časy obsluhy jsou nezávislé.
    • Systém má (teoreticky) nekonečně paralelních obslužných míst (každý příchozí je rovnou obsluhován).
    Označme Nt​ počet zákazníků v systému v čase t. Předpokládejme, že na začátku je systém prázdný, tj. N0​=0.
    """

    pt_2()
