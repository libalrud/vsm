def X(K, L):
    """k... day of month, l...length of surname"""

    return ((K * L * 23) % 20) + 1


def Y(K, L):
    """k... day of month, l...length of surname"""
    return ((X(K, L) + ((K * 5 + L * 7) % 19)) % 20) + 1


def open_file(number):
    s = f"../source/{number:03d}.txt"
    f = open(s, "r").read()

    # todo check f
    return f


def data_1(K, L):
    x = X(K, L)
    data = open_file(x)
    return [x] + data.split("\n")


def data_2(K, L):
    y = Y(K, L)
    data = open_file(y)
    return [y] + data.split("\n")
