from matplotlib import pyplot as plt
from utils import *
from graph_utils import *


def plot_frequency_histogram(fig, histogram: dict[str], name: str, which: int):
    frequency_histogram(fig, histogram, name, which, possible_values=all_chars())




def pt_1(data1: tuple[int, str, str], data2: tuple[int, str, str]):
    """(1b) Z obou datových souborů načtěte texty k analýze.
    Pro každý text zvlášť odhadněte pravděpodobnosti znaků (symbolů včetně mezery), které se v textech vyskytují.
    Výsledné pravděpodobnosti graficky znázorněte.
    """

    # Possible TODO: show graph of ratio of counts

    histogram_1 = count_occurrences(data1[2])
    histogram_2 = count_occurrences(data2[2])

    fig = plt.figure(figsize=(10, 6), dpi=100)
    plot_frequency_histogram(fig, histogram_1, data1[1], 1)
    plot_frequency_histogram(fig, histogram_2, data2[1], 2)
    plt.show()

    frequency_histogram_comparative(data1, data2, all_chars())


def pt_2(data1: tuple[int, str, str], data2: tuple[int, str, str]):
    """(1b) Pro každý text zvlášť spočtěte entropii odhadnutého rozdělení znaků."""

    frequencies1 = pdf_estimate(data1[2])
    frequencies2 = pdf_estimate(data2[2])

    print(f"H(data1) = {entropy(frequencies1)} bits")
    print(f"H(data2) = {entropy(frequencies2)} bits")


def pt_3(data1: tuple[int, str, str], data2: tuple[int, str, str]):
    """
    (2b) Nalezněte optimální binární instantní kód CC pro kódování znaků prvního z textů.
    Řádně vysvětlete, proč je vámi nalezený kód optimální!
    """

    # huffman
    # encoding = dictionary[char, string of 01's]

    frequencies1 = pdf_estimate(data1[2])
    frequencies2 = pdf_estimate(data2[2])

    huff1 = huffman(frequencies1)
    huff2 = huffman(frequencies2)

    print(huff1)
    print(huff2)

    fig = plt.figure(figsize=(10, 6), dpi=100)
    ax = fig.add_subplot(1, 1, 1)

    values = all_chars()
    x_coordinates = range(len(values))

    frequencies1 = [len(huff1[x]) for x in values]
    frequencies2 = [len(huff2[x]) for x in values]

    ax.bar(x_coordinates, frequencies1, align='center', color='b', alpha=.4, label=f"{data1[1]}")
    ax.bar(x_coordinates, frequencies2, align='center', color='r', alpha=.4, label=f"{data2[1]}")

    plt.xlabel("char")
    plt.ylabel("Huffman code length")

    ax.legend()

    ax.set_title(f"Huffman code length for {data1[1]} vs {data2[1]}")

    ax.xaxis.set_major_locator(plt.FixedLocator(x_coordinates))
    ax.xaxis.set_major_formatter(plt.FixedFormatter(values))
    plt.show()


def pt_4(data1: tuple[int, str, str], data2: tuple[int, str, str]):
    """Pro každý text zvlášť spočtěte střední délku kódu CC
    a porovnejte ji s entropií rozdělení znaků.
    Je kód CC optimální i pro druhý text? Řádně zdůvodněte!"""


    pdf1 = pdf_estimate(data1[2])
    pdf2 = pdf_estimate(data2[2])

    huff1 = huffman(pdf1)
    print(f"Huffifying {data2[1]}")
    huff2 = huffman(pdf2)

    lc1_huff1 = mean_length(huff1, pdf1)
    lc2_huff1 = mean_length(huff1, pdf2)

    lc1_huff2 = mean_length(huff2, pdf1)
    lc2_huff2 = mean_length(huff2, pdf2)

    print(f"For set 1, huffman coding 1's average length is {lc1_huff1} and huffman coding 2's is {lc1_huff2}")
    print(f"For set 2, huffman coding 1's average length is {lc2_huff1} and huffman coding 2's is {lc2_huff2}")


def hw1(data1, data2):
    pt_1(data1, data2)
    pt_2(data1, data2)
    pt_3(data1, data2)
    pt_4(data1, data2)
