from data import *
from hw1 import *
from hw2 import *
from hw3 import *
from hw4 import *


def main():
    # K, L inputs
    K = 27
    L = 5

    print(f"{K=} {L=}, X={X(K, L)} Y={Y(K, L)}")

    # data in 3-tuple (number of dataset, 1st line, 2nd line)
    data1 = data_1(K, L)
    data2 = data_2(K, L)

    # print info
    # print(data1[:2])
    # print(data2[:2])
    # print()

    # hw1(data1, data2)
    hw4(data1, data2)


if __name__ == "__main__":
    main()
