from matplotlib import pyplot as plt
from utils import *
from graph_utils import *
import scipy as sp


def plot_frequency_histogram(fig, histogram: dict[Any], name: str, which: int):
    frequency_histogram(fig, histogram, name, which, possible_values=[i for i in range(1, 1 + max(histogram.keys()))])

def plot_frequency_histogram_comparative(data1, data2):
    """
    Plot both histograms next to each other
    """

    fig = plt.figure(figsize=(10, 6), dpi=100)
    ax = fig.add_subplot(1, 1, 1)

    longest_word = max(get_longest_word_length(data1[2]), get_longest_word_length(data2[2]))
    word_lengths = [i for i in range(1, 1 + longest_word)]
    values = word_lengths
    x_coordinates = range(len(values))

    frequencies1 = normalized_word_lengths(data1[2], longest_word).values()
    frequencies2 = normalized_word_lengths(data2[2], longest_word).values()

    ax.bar(x_coordinates, frequencies1, align='center', color='b', alpha=.4, label=f"{data1[1]}")
    ax.bar(x_coordinates, frequencies2, align='center', color='r', alpha=.4, label=f"{data2[1]}")

    plt.xlabel("word length")
    plt.ylabel("log frequency (%)")

    ax.legend()
    # ax.set_yscale('log')
    ax.set_title(f"{data1[1]} vs {data2[1]}")

    ax.xaxis.set_major_locator(plt.FixedLocator(x_coordinates))
    ax.xaxis.set_major_formatter(plt.FixedFormatter(values))
    plt.show()


def pt_1(data1, data2):
    """
    (1b) Z obou datových souborů načtěte texty k analýze.
    Pro každý text zvlášť odhadněte pravděpodobnosti délek
    slov a graficky znázorněte rozdělení délek slov.
    """

    # total word counts
    data1_WC = count_words(data1[2])
    data2_WC = count_words(data2[2])

    # word length counts
    data1_WLC = {i: val for i, val in enumerate(count_word_lengths(data1[2]))}
    data2_WLC = {i: val for i, val in enumerate(count_word_lengths(data2[2]))}

    fig = plt.figure(figsize=(10, 6), dpi=100)
    plot_frequency_histogram(fig, data1_WLC, data1[1], 1)
    plot_frequency_histogram(fig, data2_WLC, data2[1], 2)
    plt.show()

    plot_frequency_histogram_comparative(data1, data2)

def pt_2(data1, data2):
    """
    (1b) Pro každý text zvlášť odhadněte základní charakteristiky délek slov,
    tj. střední hodnotu a rozptyl. Řádně vysvětlete, jak charakteristiky odhadujete!
    """

    vp_1 = estimate_expected_value(data1[2])
    vp_2 = estimate_expected_value(data2[2])

    vr_1 = estimate_variance(data1[2])
    vr_2 = estimate_variance(data2[2])

    print(f"WC 1 {data1[1]}: {count_words(data1[2]):.3f}")
    print(f"WC 2 {data2[1]}: {count_words(data2[2]):.3f}")

    print(f"Vyberovy prumer pro {data1[1]}: {vp_1:.3f}")
    print(f"Vyberovy rozptyl pro {data1[1]}: {vr_1:.3f}")
    print(f"Vyberovy prumer pro {data2[1]}: {vp_2:.3f}")
    print(f"Vyberovy rozptyl pro {data2[1]}: {vr_2:.3f}")


def pt_3(data1, data2):
    """
    (2b) Na hladině významnosti 5% otestujte hypotézu, že rozdělení délek slov nezávisí na tom,
    o který jde text. Určete také p-hodnotu testu. Nápověda: Lze provést testem nezávislosti
    v kontingenční tabulce. Řádně popište, jakou hypotézu testujete!
    """

    max_length = max(get_longest_word_length(data1[2]), get_longest_word_length(data2[2]))

    lengths = [i+1 for i in range(max_length)]
    data1_WC = count_word_lengths(data1[2], max_length+1)[1:]
    data2_WC = count_word_lengths(data2[2], max_length+1)[1:]

    marg_WC = [data1_WC[i-1] + data2_WC[i-1] for i in lengths]
    marg_12 = [count_words(data1[2]), count_words(data2[2])]

    n = sum(marg_12)

    print(f"{lengths=}")
    print(f"{data1_WC=}")
    print(f"{data2_WC=}")
    print(f"{marg_WC=}")
    print(f"{marg_12=}")

    matrix = [data1_WC, data2_WC]

    chi2 = 0
    for i in range(0, max_length):
        for j in range(0, 2):
            coeff_ij = marg_WC[i] * marg_12[j] / n
            value_ij = (matrix[j][i] - coeff_ij) ** 2 / coeff_ij
            chi2 += value_ij

    print(f"chi2 value: {chi2:.3f}")

    a = 0.001786
    df = (max_length - 1) * (2 - 1)

    print(f"values for chi2: {a=} {df=}")

    critical = sp.stats.chi2.isf(a, df)

    print(f"critical value: {critical:.3f}")




def hw2(data1, data2):
    """Calculate things from hw2"""

    # pt_1(data1, data2)
    # pt_2(data1, data2)
    pt_3(data1, data2)


