import numpy as np
from matplotlib import pyplot as plt
from utils import *
from graph_utils import *
import scipy as sp
import seaborn as sns


def plot_frequency_histogram(fig, histogram: dict[Any], name: str, which: int):
    frequency_histogram(fig, histogram, name, which, possible_values=[i for i in range(1, 1 + max(histogram.keys()))])

def plot_frequency_histogram_comparative(data1, data2):
    """
    Plot both histograms next to each other
    """

    fig = plt.figure(figsize=(10, 6), dpi=100)
    ax = fig.add_subplot(1, 1, 1)

    longest_word = max(get_longest_word_length(data1[2]), get_longest_word_length(data2[2]))
    word_lengths = [i for i in range(1, 1 + longest_word)]
    values = word_lengths
    x_coordinates = range(len(values))

    frequencies1 = normalized_word_lengths(data1[2], longest_word).values()
    frequencies2 = normalized_word_lengths(data2[2], longest_word).values()

    ax.bar(x_coordinates, frequencies1, align='center', color='b', alpha=.4, label=f"{data1[1]}")
    ax.bar(x_coordinates, frequencies2, align='center', color='r', alpha=.4, label=f"{data2[1]}")

    plt.xlabel("word length")
    plt.ylabel("log frequency (%)")

    ax.legend()
    # ax.set_yscale('log')
    ax.set_title(f"{data1[1]} vs {data2[1]}")

    ax.xaxis.set_major_locator(plt.FixedLocator(x_coordinates))
    ax.xaxis.set_major_formatter(plt.FixedFormatter(values))
    plt.show()


def graph_transition_heatmap(chances_1, possible_values):
    plt.imshow(chances_1, cmap='hot', interpolation='nearest')
    ax = plt.gca()
    plt.suptitle(f"Absolute char pair occurence count.")
    ax.xaxis.tick_top()
    ax.xaxis.set_major_locator(plt.FixedLocator(range(len(possible_values))))
    ax.xaxis.set_major_formatter(plt.FixedFormatter(possible_values))
    ax.yaxis.set_major_locator(plt.FixedLocator(range(len(possible_values))))
    ax.yaxis.set_major_formatter(plt.FixedFormatter(possible_values))
    plt.colorbar()
    # ax.set_xlabel("Next char value")
    ax.xaxis.set_label_position('top')
    ax.set_ylabel("Current char value")
    plt.show()


def pt_1(data1, data2):
    """
    (2b) Za předpokladu výše odhadněte matici přechodu markovského řetězce pro první text.
    Pro odhad matice přechodu vizte přednášku 17. Odhadnuté pravděpodobnosti přechodu vhodně
    graficky znázorněte, např. použitím heatmapy. Popište, jakou metodou parametry odhadujete!
    """

    # from set of all chars to set of all chars
    # četnost přechodů:
    # řádky jsou z, sloupce do

    print(count_occurences_array(data1[2]))
    print(count_occurences_array(data2[2]))

    # normalized counts
    chances_1 = transition_matrix(data1[2])
    possible_values = ["␣"] + all_chars()[1:]

    # print(np.asarray(all_transitions(data1[2])))

    graph_transition_heatmap(chances_1, possible_values)

    # possible TODO: test it

def pt_2(data1, data2):
    """
    (2b) Na základě matice z předchozího bodu najděte stacionární rozdělení π
     tohoto řetězce pro první text.
     Ověřte, že se skutečně jedná o stacionární rozdělení!
    """

    # slide 15/26:
    # pi = lim(n->inf, p(n)) pro libovolne p(0)


    matrix = np.asarray(transition_matrix(data1[2]))
    matrix = np.linalg.matrix_power(matrix, 100)
    matrix[np.abs(matrix) > 1e-8] = 1
    print(matrix)

    text = data1[2]

    p0 = np.asarray([1 / len(all_chars()) for _ in range(len(all_chars()))])
    p1 = get_stationary(text, p0)

    print(f"stationary vector: {p1}")


    # print(np.matmul(p1, matrix))

    # this is basically the default char frequency


def pt_3(text1, text2):
    """
    (2b) Porovnejte rozdělení znaků druhého textu se stacionárním rozdělením π,
    tj. na hladině významnosti 5 % otestujte hypotézu, že rozdělení znaků druhého
    textu se rovná rozdělení π z předchozího bodu.
    Řádně popište, jakou hypotézu testujete!
    """


    # OČEKÁVANÉ ROZDĚLENÍ pi
    pi = get_stationary(text1)

    # REÁLNÉ ČETNOSTI Ni
    Ni = np.asarray(count_occurences_array(text2))

    # Z is a problem
    # join with J
    pi[10] += pi[-1]
    Ni[10] += Ni[-1]

    n = len(text2)  # the count for the SECOND set
    total_chars = len(all_chars()) - 1  # neglect last char -- 'z'

    # chi2
    chi2 = 0
    for i in range(0, total_chars):
        npi = n * pi[i]
        chi2 += (Ni[i] - npi)**2 / npi


    print(f"chi2 value: {chi2:.3f}")

    a = 0.05
    df = (total_chars - 1)  # degrees of freedom

    print(f"values for chi2: {a=} {df=}")

    critical = sp.stats.chi2.isf(a, df)

    print(f"critical value: {critical:.3f}")

def hw3(data1, data2):
    """
    Z obou datových souborů načtěte texty k analýze. Pro každý text zvlášť zjistěte absolutní
    četnosti jednotlivých znaků (symbolů včetně mezery), které se v textech vyskytují.
    Dále předpokládejme, že první text je vygenerován z homogenního
    markovského řetězce s diskrétním časem.
    """

    # add count occurences
    data1 += [len(data1[2]), count_occurrences(data1[2])]
    data2 += [len(data2[2]), count_occurrences(data2[2])]



    pt_3(data1[2], data2[2])



